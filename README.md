# Frisbee React Native Challenge

Te presentamos nuestra evaluación para candidatos como React Native Engineers en [Frisbee App](frisbee.com.ec).


## De qué se trata esta evaluación?

Queremos darte el tiempo y la libertad de mostrarnos tus skills con un proyecto real. No creemos que la mejor forma de evaluar a un candidato es mediante una entrevista con presión de tiempo, temas desconocidos o rompecabezas matemáticos que no son relevantes para un/a miembro/a del Equipo de Ingenieria en Frisbee.

Diseñamos esta evaluación con la intención de hacer una evaluación real y poder discutir tu solución e implementación más a fondo en nuestra siguiente entrevista.

Creemos que el desafío no debería demorarte más de un día para completar, asi que utiliza eso como una métrica de tu tiempo.

El proyecto inicial tiene la estructura de carpetas como consideramos que es una buena práctica de tener las aplicaciones, te recomendamos mantener esa estructura para que el proceso de evaluación sea más efectivo.

Esta aplicación es hecha con Expo para facilitar el proceso de build y desarrollo.

Esperamos la disfrutes!

## Tecnologías:
- Expo SDK36 with TypeScript
- Apollo Client v3 with mocking
- React Navigation v5
## Desafío

Vamos a utlizar la API en GraphQL de [`Countries`](https://github.com/lennertVanSever/graphcountries). Es un servicio sencillo de utilizar pero muy poderoso para recibir información de países alrededor del mundo.

El desafío es crear una aplicación sencilla con dos tabs utilizando este servicio y aprender un poco más de los países en el mundo!

## Funcionalidad

### Tab #1: Lista de Países:

- La primera tab debe tener una lista (scrollable) de los países ordenados alfabéticamente. No es necesario implementar paginación, si quieres hacerlo, no hay problema pero no es necesario.

- Cada item de la lista debe tener la siguiente información: Nombre del País, Bandera, Capital y Población. No tenemos un pedido exacto del layour de cada fila, pero entre mejor se vea va a ser mejor!

- Debemos tener una search box arriba de la lista donde puedas buscar los países por nombre, no es necesario hacer la búsqueda en la API, la búsqueda puede ser localmente en el dispositivo.

- Al darle tap a cada país, debemos navegar a una nueva página con más información acerca del país: Nombre, Bandera, Capital, Población, moneda, y timezone. Si crees que más información puede ser relevante y la puedes mostrar de una manera amigable y clara añádela por favor.

- Debemos incluir la flecha de `Atrás` para volver a la lista de países.

Para toda esta tab, solo debes usar la query `Countries` de la API.

### Tab #2: Distancia entre Países:

En esta tab estamos interesados en ver claramente los 5 países más cercanos de cada país.

Vamos a tener dos Listas horizontales una arriba de la otra.

- La primera lista es la list principal, vamos a mostrar cada país con su bandera. El país que se encuentre en el centro debe tener una bandera y un nombre más grande que los otros países.

- Cuando un país esté seleccionado abajo tiene que haber otra lista horizontal, mostrando los 5 países más cercanos al país seleccionado. Cada país en la lista de abajo también tiene que tener su bandera, nombre y capital. Te recomendamos que en la segunda lista los tamaños de los componentes sean más pequeños.


### Componentes UI

Vamos a utilizar los componentes de `React Native UI Kitten`

- Documentación: https://akveo.github.io/react-native-ui-kitten/docs/getting-started/what-is-ui-kitten#what-is-ui-kitten

Te invitamos a que leas la documentación, lo instales en el proyecto y utilices estos componentes en tu solución.


### GraphQL
El servicio de `Countries` lo vamos a tener del siguiente repositorio:

- Repositorio: https://github.com/lennertVanSever/graphcountries
- GraphQL 

En este proyecto inicial ya está configurado la conexión con Apollo Client, también tenemos configurado el URL de Countries, pero si lo necesitas este es el URL al que debes dirigir tus peticiones de GraphQL:

- https://countries-274616.ew.r.appspot.com/

Si no estás tan familarizado/a con GraphQL te invitamos a que leas más en su documentación oficial, así como Apollo Client:

- GraphQL Documentación: https://graphql.org/learn/
- Apollo Client: https://www.apollographql.com/docs/tutorial/introduction/




## Evaluación
- 30% - UI y UX
- 40% - Diseño General y Estructura
- 30% - Data Management y Algoritmos



## Como correr el proyecto

Debes tener Node.js y NPM instalado localmente.

```bash
$ git clone git@gitlab.com:sebasalvarado/frisbee-react-native-challenge.git
$ cd frisbee-react-native-challenge
$ npm install -g expo-cli
$ npm install
$ npm run ios
```

Si tienes algún problema corriendo el simulador de iOS, es posible que aquí encuentres la solución: https://github.com/facebook/react-native/issues/23282

## Cómo Enviar tu Solución:

Crea una branch en tu proyecto local con el siguiente formato `nombre-apellido`, después de revisarlo juntos en la siguiente entrevista vas a poder subirlo al repositorio remoto. 